;;; exwm-firefox--tests.el --- Tests for exwm-firefox  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Ian Eure

;; Author: Ian Eure <ieure@l0p>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'ert)
(require 'exwm-firefox)

(ert-deftest exwm-firefox--test--exwm-firefox? ()
  (should (let ((exwm-class-name "Mozilla Firefox")) (exwm-firefox?)))
  (should (let ((exwm-class-name "LibreWolf")) (exwm-firefox?))))

(ert-deftest exwm-firefox--test--class+title->buffer-name ()
  (let ((testcases
         ;;  Expected buffer name              class         title
         '(("*firefox*"                               "Firefox-esr"     "Mozilla Firefox")
           ("*firefox: Codeberg.org*"                 "Firefox-esr"     "Codeberg.org — Mozilla Firefox")
           ("*firefox-private*"                       "Firefox-esr"     "Mozilla Firefox (Private Browsing)")
           ("*firefox-private: Codeberg.org*"         "Firefox-esr"     "Codeberg.org — Mozilla Firefox (Private Browsing)")
           ("*firefox: Home · retro.social*"          "firefox-default" "Home · retro.social — Nightly")

           ("*librewolf*"                       "librewolf-default" "LibreWolf")
           ("*librewolf: Codeberg.org*"         "librewolf-default" "Codeberg.org — LibreWolf")
           ("*librewolf-private*"               "librewolf-default" "LibreWolf Private Browsing")
           ("*librewolf-private: Codeberg.org*" "librewolf-default" "Codeberg.org — LibreWolf Private Browsing")
           ("*librewolf: emacs-weirdware/exwm-firefox: Enhanced support for Firefox under EXWM - exwm-firefox - Codeberg.org*" "librewolf-default" "emacs-weirdware/exwm-firefox: Enhanced support for Firefox under EXWM - exwm-firefox - Codeberg.org — LibreWolf")

           ("*icecat*"                          "icecat-default"    "GNU IceCat")
           ("*icecat-private*"                  "icecat-default"    "GNU IceCat Private Browsing")
           ("*icecat-private: Codeberg.org*"    "icecat-default"    "Codeberg.org — GNU IceCat Private Browsing")
           ("*icecat: Codeberg.org*"            "icecat-default"    "Codeberg.org — GNU IceCat"))))

    (dolist (testcase testcases)
      (cl-destructuring-bind (expected class title) testcase
        (should (string= expected (exwm-firefox--class+title->buffer-name class title)))))))

(provide 'exwm-firefox--tests)
;;; exwm-firefox--tests.el ends here
